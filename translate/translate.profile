<?php

/**
 * @file
 * Enables modules and site configuration for the profile.
 */

use Drush\Log\LogLevel;

/**
 * Implements hook_install_tasks().
 */
function translate_install_tasks(&$install_state) {
  $tasks = [
    'translate_install_profile_modules' => [
      'display_name' => t('Install translate modules'),
      'type' => 'batch',
      'display' => TRUE,
    ],
#    'translate_translations' => [
#      'display_name' => t('Update translations'),
#      'type' => 'normal',
#      'display' => TRUE,
#    ],
    'translate_theme_setup' => [
      'display_name' => t('Apply theme'),
      'display' => TRUE,
    ],
  ];
  return $tasks;
}

/**
 * Installs required modules via a batch process.
 *
 * @param array $install_state
 *   An array of information about the current installation state.
 *
 * @return array
 *   The batch definition.
 *
 * @NOTE: This function is copied directly from
 *        social_install_profile_modules().
 */
function translate_install_profile_modules(array &$install_state) {
  translate_log('Preparing to install profile modules');
  $files = system_rebuild_module_data();

  $modules = $translate_modules = [
    'settlement_multilingual' => 'settlement_multilingual',
    'settlement_translate_test' => 'settlement_translate_test',
  ];

  // Always install required modules first. Respect the dependencies between
  // the modules.
  $required = [];
  $non_required = [];

  // Add modules that other modules depend on.
  foreach ($modules as $module) {
    if ($files[$module]->requires) {
      $module_requires = array_keys($files[$module]->requires);
      // Remove the social modules from required modules.
      $module_requires = array_diff_key($module_requires, $translate_modules);
      $modules = array_merge($modules, $module_requires);
    }
  }
  $modules = array_unique($modules);
  // Remove the social modules from to install modules.
  $modules = array_diff_key($modules, $translate_modules);
  foreach ($modules as $module) {
    if (!empty($files[$module]->info['required'])) {
      $required[$module] = $files[$module]->sort;
    }
    else {
      $non_required[$module] = $files[$module]->sort;
    }
  }
  arsort($required);

  $operations = [];

  $operations[] = [
    '_translate_install_module_batch',
    [array_keys($required), implode(', ', array_keys($required))],
  ];

  $operations[] = [
    '_translate_install_module_batch',
    [array_keys($non_required), implode(', ', array_keys($non_required))],
  ];

  foreach ($translate_modules as $module => $weight) {
    $operations[] = [
      '_translate_install_module_batch',
      [[$module], $module],
    ];
  }

  $batch = [
    'operations' => $operations,
    'title' => t('Install translate modules'),
    'error_message' => t('The installation has encountered an error.'),
  ];
  return $batch;
}

/**
 * Implements callback_batch_operation().
 *
 * Performs batch installation of modules.
 */
function _translate_install_module_batch($module, $module_name, &$context) {
  set_time_limit(0);
  \Drupal::service('module_installer')->install($module);
  translate_log('Installed module(s): ' . $module_name);
  $context['results'][] = $module;
  $context['message'] = t('Install %module_name module.', ['%module_name' => $module_name]);
}

function translate_log($message = '', $type = LogLevel::NOTICE) {
  if (function_exists('drush_log')) {
    drush_log($message, $type);
  }
}

/**
 * Manually trigger config translation overrides to impor
 * TODO: trigger locale-update process here as well?
 */
function translate_translations() {
  \Drupal::service('language.config_factory_override')
    ->installLanguageOverrides( 'fr' );
  drupal_flush_all_caches();
}


function translate_theme_setup(array &$install_state) {
  // Clear all status messages generated by modules installed in previous steps.
  drupal_get_messages('status', TRUE);

  $themes = ['bartik'];
  \Drupal::service('theme_handler')->install($themes);
  translate_log('Installed theme(s): ' . implode(', ', $themes));

  \Drupal::configFactory()
    ->getEditable('system.theme')
    ->set('default', 'bartik')
    ->save();

  // Ensure that the install profile's theme is used.
  // @see _drupal_maintenance_theme()
  \Drupal::service('theme.manager')->resetActiveTheme();
  translate_log('Set default theme.');
}

