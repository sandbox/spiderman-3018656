#include .mk/GNUmakefile

# Set some variables for use across commands.
SITE_URL  = translate.lndo.site
TIMESTAMP = $(shell date +%s)
TMP_DIR   = tmp

# Suppress Make-specific output, but allow for greater verbosity.

# Normalize local development and CI commands.
LANDO = $(shell which lando)
ifeq ($(LANDO),)
    DRUSH_CMD = ./bin/drush
    APP_PATH  =
else
    DRUSH_CMD = $(LANDO) drush
    APP_PATH  = /app/
endif
DRUSH = $(DRUSH_CMD) --uri=$(SITE_URL)

# Colour output. See 'help' for example usage.
ECHO       = @echo -e
BOLD       = \033[1m
RESET      = \033[0m
make_color = \033[38;5;$1m  # defined for 1 through 255
GREEN      = $(strip $(call make_color,22))
GREY       = $(strip $(call make_color,241))
RED        = $(strip $(call make_color,124))
WHITE      = $(strip $(call make_color,255))
YELLOW     = $(strip $(call make_color,94))

SITE_PROFILE := translate

#include scripts/makefiles/*.mk

.PHONY: drumkit help all
.DEFAULT_GOAL = help


drumkit:
	@if [[ ! "${DRUMKIT}" -gt 0 ]]; then echo -e "$(BOLD)$(WHITE)Remember to bootstrap Drumkit ($(GREEN). d$(WHITE))$(RESET)"; fi
	@if [[ ! "${DRUMKIT}" -gt 0 ]]; then git submodule update --init; fi

help: drumkit
	@$(ECHO) "$(BOLD)$(GREY)Available 'make' commands:$(RESET)"
	@$(ECHO) "  $(GREEN)all$(RESET)"
	@$(ECHO) "    $(YELLOW)Start containers, build platform and install sites.$(RESET)"

all:
	start
	build
	install

# Build (and clean up) a Drupal codebase.

build:
	mkdir -p vendor
	$(LANDO) composer --ansi create-project --no-progress
	@$(ECHO) "$(YELLOW)Completed build of codebase.$(RESET)"

# Install (and clean up) Drupal site.

MYSQL_DATABASE ?= translate
MYSQL_USER     ?= translate
MYSQL_PASSWORD ?= translate

SITE_INSTALL_CMD = site:install translate \
                       --site-name="Translate test" \
                       --yes --locale="en" \
                       --db-url="mysql://$(MYSQL_USER):$(MYSQL_PASSWORD)@translate_db/$(MYSQL_DATABASE)" \
                       --sites-subdir=$(SITE_URL) \
                       --account-name="dev" \
                       --account-mail="dev@$(SITE_URL)" \
                       --account-pass="pwd"

install:
	@$(ECHO) "$(YELLOW)Beginning installation of $(GREY)$(SITE_URL)$(YELLOW). (Be patient. This may take a while.)$(RESET)"
	$(DRUSH) $(SITE_INSTALL_CMD)
	@$(ECHO) "$(YELLOW)Completed installation of $(GREY)$(SITE_URL).$(RESET)"

uninstall:
	$(DRUSH) -y sql:drop
	@$(ECHO) "$(YELLOW)Deleted $(GREY)$(SITE_URL).$(RESET)"

locale:
	$(DRUSH) locale-check && $(DRUSH) locale-update && $(DRUSH) cr
